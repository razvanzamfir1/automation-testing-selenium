from selenium import webdriver  # webdriver ne lasa sa conducem webbrouserul prin intermediul codului
from selenium.webdriver.common.by import By
import time

# Selenium ne ajuta sa interactionam cu un browser fara interactiune umana, sa automatizam
#la inceput trebuie sa instalam driver-ul specific pentru webrowser-ul cu care lucram: chromedriver -> later stable release -> chromedriver win32 zip->
# apoi mutam chromedriver-ul in folderul unde se afla file-ul rulat din IDE

# 1. crearea legaturii aplicatiei cu browser-ul

chrome_browser = webdriver.Chrome('./chromedriver')  # crrearea instantei pentru browser Chrome este browser-ul nostru .Chorme (este clasa pe care ne-o da webdriver-ul; cromedriver =our driver
chrome_browser.maximize_window()  # deschide pagina de chrome pe tot ecranul; atentie actualizare chrome cu ultima versiune; de fiecare data cand rulam codul creem o noua instanta a browserului
chrome_browser.get('https://demo.seleniumeasy.com/basic-first-form-demo.html')  # pentru a deschide o anumita pagina

# assert'Selenium Easy Demo' in chrome_browser.title  # se verifica daca suntem in pagina corecta, daca exista in titlul paginii acest nume

# Nota - cu selenium putem lua, modifica orice element HTML gasit in pagina HTML a unui website. putem face asta foarte rapid fata de un om cu automatizarea
# Selectori sunt unelete pentru a selecta/lua lucruri de pe pagina.
# daca ne uitam la sfarsitul uni tag HTML vom vedea de ex $0 care ne ajuta sa ascoatem de pe pagina acel tag, de ex un buton

# 2. This solves the issue with the Popup for those that encounter it:

chrome_browser.implicitly_wait(2)
popup = chrome_browser.find_element(By.ID, 'at-cv-lightbox-close')
popup.click()

# 3. aici am automatizat adaugarea unui text intr-o casuta de pe pagina(tastare automata)

# assert 'Show Message' in chrome_browser.page_source  # verificam daca butonul show message este in pagina respactiva

user_message = chrome_browser.find_element(By.ID, 'user-message')  # selectam dupa id, din fisierul HTML, casuta de mesaj
user_message.clear()                                               # ne asiguram ca nu este nimic scris in casuta de input
user_message.send_keys('I AM EXTRA COOOOL')                        # adaugam textul in casuta, mimam scrierea textului de catre robot

# 4. aici am automatizat apasarea pe buton peentru afisarea mesajului in casuta de output

time.sleep(2)  # pentru a crea un timp de asteptare intre actiunile executate de program, altfel site-ul o sa isi dea seama ca este un robot si o sa il blocheze
show_message_button = chrome_browser.find_element(By.CLASS_NAME, 'btn-default')  ## selectam dupa clasa CSS, din fisierul HTML, butonul

# user_button2 = chrome_browser.find_element(By.CSS_SELECTOR, '.btn')  # cand vrem sa selectam dupa un anumit style
# user_button2 = chrome_browser.find_element(By.CSS_SELECTOR, '#get-input > .btn')  # cand vrem sa selectam toate butoanele copil din interiorul unui formular dupa id (#) formularului
# print(user_button2)

print(show_message_button.get_attribute('innerHTML'))  # innerHTML ia tot ce se afla in interiorul tagu-ului HTML, de exemplu textul butonului show message
show_message_button.click()  # methoda click simuleaza apasarea butonului, de catre robot

# 5. pentru a lua informatia afisata in casuta de output

output_message = chrome_browser.find_element(By.ID, 'display')  # selectam dupa id, din fisierul HTML, casuta de output
assert 'I AM EXTRA COOOOL' in output_message.text               # verificam daca exista mesajul in casuta de output; se poate pune fie .text fie innerHTML

# 6. inchidere fereasta/ferestre si/sau browser/browsere

chrome_browser.close()  # inchide fereastra curenta pe care o dechidem, uneori mai da eroare, in functie de browser, este bine sa fie scrisa de doua ori linia
chrome_browser.quit()   # Inchidem intregul chrome browser, daca a mai fost deschisa o alta sesiune anterior, in afara de cea curenta o inchide si pe ea.

# Nota finala: selenium este foarte puternic pentru ca putem crea roboti cu el care sa ia informatii de pe site-uri
# Ex: un robot care sa dea like pe facebook, in mod continuu, la infinit
# Ex pe un site oamenii in mod aleatoriu sa se logheze si sa voteze favorabil subscrierea postata de tine
# transmiterea de mesaje personale prietenilor de pe facebook

